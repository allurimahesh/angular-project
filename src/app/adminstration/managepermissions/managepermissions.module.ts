import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagePermissionsComponent } from './managepermissions.component';
import {ManagePermissionsRoutingModule} from './managepermissions-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {HttpModule} from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    ManagePermissionsRoutingModule,
    SharedModule,
    HttpModule
  ],
  declarations: [ManagePermissionsComponent]
})
export class ManagePermissionsModule { }
