import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManagePermissionsComponent} from './managepermissions.component';

const routes: Routes = [
  {
    path: '',
    component: ManagePermissionsComponent,
    data: {
      breadcrumb: 'ManagePermissions',
      title: 'Managepermissions'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagePermissionsRoutingModule { }
