import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManageRolesComponent} from './manageroles.component';
import {ManageRolesRoutingModule} from './manageroles-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {HttpClientModule} from '@angular/common/http';
import {RoleService} from './role.service'
import {FormsModule} from '@angular/forms'
import { HttpModule } from '@angular/http';

@NgModule({
    imports:[
        CommonModule,
        SharedModule,
        HttpClientModule,
        FormsModule,
        HttpModule,
        ManageRolesRoutingModule,
     ],

     declarations:[ManageRolesComponent],
     providers:[RoleService],
     })

    export class ManageRolesModule { } 
