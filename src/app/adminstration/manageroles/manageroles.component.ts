import { Component, OnInit, Input, ViewEncapsulation} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RoleService } from './role.service';
import { Role } from './role';
import swal from 'sweetalert2';
import {fadeInOutTranslate} from '../../shared/elements/animation';

@Component({
  selector: 'app-manageroles',
  templateUrl: './manageroles.component.html',
  styleUrls: ['./manageroles.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInOutTranslate]
})

export class ManageRolesComponent implements OnInit {
  showDialog = false;
  @Input() visible: boolean;
  public config: any;

  statusCode: number;
  processValidation = false;
  roleIdToAdded = null;

  public data: any;
  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  public alertRes: any;

  public rolesData: any = {
    'role': '',
    'description': '',
    'id': ''
  };

  public addData: any = {
    'role': '',
    'description': ''
  }

  roleForm = new FormGroup({
    role: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  constructor(private roleService:RoleService) {}

  ngOnInit(): void {
    this.getAllRoles();
  }

  public addRoles(data, saveId) {
    document.getElementById(saveId).classList.add('md-show');
    if (this.roleForm.valid) {
      if (this.roleIdToAdded === null) {
        let roles = new Role(this.roleIdToAdded, data.role, data.description);
        roles.id = this.roleIdToAdded;
        this.roleService.createRole(roles)
          .subscribe(data => { 
            document.getElementById(saveId).classList.remove('md-show')
            this.getAllRoles()
            swal({
              title: 'Good job!',
              text: 'Your file saved Successfully!',
              type: 'success'
            }).catch(swal.noop);
              },
            error => error)
      } 
     };
      this.addData.role = '';
      this.addData.description = '';
    }

  /**
   * Edit method
   * @param data 
   */
  public editRole(eleId, roleData) {
      document.getElementById(eleId).classList.add('md-show');
      this.rolesData = roleData;
  }
 
  /**
   * updateData
   */

  public updateData(eleId, newData) {
    let roles: any = newData;
    this.roleService.editRoleById(roles)
      .subscribe(success => {
        this.getAllRoles();
        document.getElementById(eleId).classList.remove('md-show');
        // this.toastr.info('Role Updated Succesfully!');
        swal({
          title: 'Good job!',
          text: 'Your file Updated Successfully!',
          type: 'success'
        }).catch(swal.noop);
      },
        error => error);
  }

  getAllRoles(): any {
    this.roleService.getAllRoles()
      .subscribe(
        (data) => {
          this.data = data,
            errorCode => this.statusCode = errorCode
        });
  }

  deleteRole(role: string) {
    swal({title: 'Are you sure?',
      text: 'You want to delete',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.roleService.deleteRoleById(role)
        .subscribe(success => {
          this.getAllRoles();
        swal(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        );
      })
  };
})
  }

  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }

  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement.parentElement).classList.remove('md-show');
  }

  closeModal(eventId) {
    document.getElementById(eventId).classList.remove('md-show');
  }
  /**
 * Delete Alert
 **/

}
