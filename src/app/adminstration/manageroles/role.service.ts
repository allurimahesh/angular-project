import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs';
// import {Role} from './role';

@Injectable()

export class RoleService {

    rolesUrl= "http://localhost:3000/Role";

constructor(private http:Http){}

getAllRoles(): Observable<number> {
  // console.log(this.rolesUrl, Role);
  return this.http.get(this.rolesUrl)
   .map(this.extractData)
    .catch(this.handleError);
}

createRole(roles):Observable<number> {
  let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: cpHeaders });
    return this.http.post(this.rolesUrl, roles, options)
           .map(success => success.status)
           .catch(this.handleError);
}

 editRoleById(roles: any): Observable<number> {
    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: cpHeaders });
    return this.http.put(this.rolesUrl + "/" + roles.id, JSON.stringify(roles), options)
   .map(success => success.status)
   .catch(this.handleError);
  }

deleteRoleById(roles: string): Observable<number> {
  let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: cpHeaders });
  return this.http.delete(this.rolesUrl +"/"+ roles)
   .map(success => success.status)
   .catch(this.handleError);
} 

private extractData(res: Response) {
  let body = res.json();
    return body;
}
private handleError (error: Response | any) {
return Observable.throw(error.status);
}

}