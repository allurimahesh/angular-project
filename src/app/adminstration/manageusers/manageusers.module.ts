import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageUsersComponent } from './manageusers.component';
import {ManageUsersRoutingModule} from './manageusers-routing.module'
import {SharedModule} from '../../shared/shared.module';
import {HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {UserService} from './user.service'
import { FormsModule,ReactiveFormsModule} from '@angular/forms'
import {ToastModule} from 'ng2-toastr/ng2-toastr'


@NgModule({
  imports: [
    CommonModule,
    ManageUsersRoutingModule,
    SharedModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule.forRoot(),
  ],
  declarations: [ManageUsersComponent],
  providers:[UserService]
})
export class ManageUsersModule { }
