import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs';
import { Options } from 'selenium-webdriver/chrome';


@Injectable()
export class UserService {

  usersUrl = "http://localhost:3000/User";

  constructor(private http:Http) { }

  getAllUsers(): Observable<number> {
    return this.http.get(this.usersUrl)
    .map(this.extractData)
    .catch(this.handleError);
  }

  createUser(users): Observable <number> {
    let cpHeaders = new Headers ({'content-type':'application/json'});
    let options = new RequestOptions({ headers: cpHeaders});
    return this.http.post(this.usersUrl, users, options)
    .map(success => success.status)
    .catch(this.handleError)
  }

  editUserById(users): Observable <number>{
    let cpHeaders = new Headers({'content-type': 'application/json'});
    let options = new RequestOptions ({headers:cpHeaders});
    return this.http.put(this.usersUrl + "/" + users.id, JSON.stringify(users), options)
      .map(this.extractData)
      .catch(this.handleError)
  }

  deleteUsersById(users: string): Observable<number>{
    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: cpHeaders });
    return this.http.delete(this.usersUrl +"/"+ users)
    .map(this.extractData)
    .catch(this.handleError)
  }

  private extractData(res: Response) {
    let body = res.json();
      return body;
  }
  private handleError (error: Response | any) {
  return Observable.throw(error.status);
  }
}

