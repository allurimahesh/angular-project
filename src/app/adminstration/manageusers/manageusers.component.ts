import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from './user.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-manageusers',
  templateUrl: './manageusers.component.html',
  styleUrls: ['./manageusers.component.scss']
})

export class ManageUsersComponent implements OnInit {

  public data: any;
  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';

  public statusCode: number;
  userIdToAddData = null;


  public userAddData: any = {
    'EmployeeID':'',
    'emailID':'',
    'department':'',
    'password':'',
    'role':''
  }

  public usersEditData: any ={
    'id' : '',
    'EmployeeID':'',
    'emailID':'',
    'department':'',
    'password':'',
    'role':''
  }
 
   userForm = new FormGroup({
    EmployeeID : new FormControl('', Validators.required),
    // emailID: new FormControl('', [Validators.required, Validators.pattern("[^ @]*@[^ @]*")]),
    emailID: new FormControl('', Validators.required),
    department: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required)
  });

  constructor(private userService:UserService, private toastr: ToastsManager , private vcr: ViewContainerRef) {this.toastr.setRootViewContainerRef(vcr);}

  ngOnInit() : void {
    this.getUsersData();
  // this.closeModal();
  }
  
  getUsersData(): any {
   // console.log(this.data + + 'form is saved'),
    this.userService.getAllUsers()    
    .subscribe((data) => 
    this.data = data,
    errorCode => this.statusCode = errorCode);
  }

  public userDataSubmit(saveId){
    document.getElementById(saveId).classList.add('md-show')
    if(this.userForm.valid){
      if(this.userIdToAddData === null){
        //let users =new User(this.userIdToAddData, data.EmployeeID, data.emailID, data.department, data.password, data.role);
        // users.id = this.userIdToAddData;
        let users: any = {'id': this.userIdToAddData,
                           'EmployeeID': this.userForm.value.EmployeeID,
                           'emailID': this.userForm.value.emailID,
                           'department':this.userForm.value.department,
                           'password':this.userForm.value.password,
                           'role':this.userForm.value.role
                            }
        this.userService.createUser(users)
            .subscribe(data => {
              document.getElementById(saveId).classList.remove('md-show')
                this.getUsersData();
                this.userForm.reset();
            },     
            error => error)    
      }
    };
    this.userAddData.EmployeeID = '';
    this.userAddData.emailID = '';
    this.userAddData.department = '';
    this.userAddData.password = '';
    this.userAddData.role = '';
  }
  
  /**
   * Edit method
   * @param data 
   */

  public editUser(eleId, userData){
    document.getElementById(eleId).classList.add('md-show');
      this.usersEditData = userData;
  }

  public userUpdateData(eleId, newData){
        let users :any = newData
         this.userService.editUserById(users)
        .subscribe( success => {
        this.getUsersData();
       document.getElementById(eleId).classList.remove('md-show')
       },
       error => error)
    }

    deleteUser(user){
      this.userService.deleteUsersById(user)
      .subscribe(success =>{
        this.getUsersData();
        this.toastr.warning('User Deleted Succesfully');
    error=> error});
    }

  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }

  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement.parentElement).classList.remove('md-show');
  }
  closemyModal(event){
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }
  closeModal(event){
    document.getElementById(event).classList.remove('md-show')
  }
}
