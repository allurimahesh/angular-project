import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChangePasswordComponent} from '../../auth/changepassword/changepassword.component';

const routes: Routes = [
  {
    path: '',
    component: ChangePasswordComponent,
    data: {
      breadcrumb: 'Password Change',
     title: 'Password Change',
      },
  }
 ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangePasswordRoutingModule { }
