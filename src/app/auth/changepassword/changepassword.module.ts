import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChangePasswordRoutingModule} from './changepassword-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {ChangePasswordComponent} from './changepassword.component'

@NgModule({
  imports: [
    CommonModule,
    ChangePasswordRoutingModule,
    SharedModule
  ],
  declarations: [ChangePasswordComponent]
})
export class ChangePasswordModule { }
