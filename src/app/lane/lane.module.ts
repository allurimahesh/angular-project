import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LaneRoutingModule } from './lane-routing.module';
import { LaneComponent } from './lane.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [CommonModule, LaneRoutingModule, SharedModule],
  declarations: [LaneComponent]
})
export class LaneModule {}
