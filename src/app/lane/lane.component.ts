import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterContentInit
} from '@angular/core';
import * as d3 from 'd3';
declare var $: any;

@Component({
  selector: 'app-lane',
  templateUrl: './lane.component.html',
  styleUrls: [
    './lane.component.scss',
    '../../assets/icon/SVG-animated/svg-weather.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class LaneComponent implements OnInit, AfterContentInit {
  datasetBarChart: any = [
    { group: 'All', category: 'V:234', measure: 78258.0845 },
    { group: 'All', category: 'V:294', measure: 60610.2355 },
    { group: 'All', category: 'V:154', measure: 30493.1686 },
    { group: 'All', category: 'V:156', measure: 56097.0151 },
    { group: 'Conveyor 1', category: 'Oranges', measure: 22913.2728 },
    { group: 'Conveyor 1', category: 'V:234', measure: 7637.7576 },
    { group: 'Conveyor 1', category: 'V:294', measure: 23549.7526 },
    { group: 'Conveyor 1', category: 'V:154', measure: 1909.4394 },
    { group: 'Conveyor 1', category: 'V:156', measure: 7637.7576 },
    { group: 'Conveyor 2', category: 'Oranges', measure: 1041.5124 },
    { group: 'Conveyor 2', category: 'V:234', measure: 2430.1956 },
    { group: 'Conveyor 2', category: 'V:294', measure: 15275.5152 },
    { group: 'Conveyor 2', category: 'V:154', measure: 4166.0496 },
    { group: 'Conveyor 2', category: 'V:156', measure: 11803.8072 },
    { group: 'Conveyor 3', category: 'Oranges', measure: 7406.3104 },
    { group: 'Conveyor 3', category: 'V:234', measure: 2545.9192 },
    { group: 'Conveyor 3', category: 'V:294', measure: 1620.1304 },
    { group: 'Conveyor 3', category: 'V:154', measure: 8563.5464 },
    { group: 'Conveyor 3', category: 'V:156', measure: 3008.8136 },
    { group: 'Conveyor 4', category: 'V:234', measure: 35411.4216 },
    { group: 'Conveyor 4', category: 'V:294', measure: 8332.0992 },
    { group: 'Conveyor 4', category: 'V:154', measure: 6249.0744 },
    { group: 'Conveyor 4', category: 'V:156', measure: 11803.8072 },
    { group: 'Conveyor 5', category: 'V:234', measure: 867.927 },
    { group: 'Conveyor 5', category: 'V:294', measure: 1808.18125 },
    { group: 'Conveyor 5', category: 'V:154', measure: 795.59975 },
    { group: 'Conveyor 5', category: 'V:156', measure: 578.618 },
    { group: 'Conveyor 6', category: 'V:234', measure: 2227.6793 },
    { group: 'Conveyor 6', category: 'V:294', measure: 3442.7771 },
    { group: 'Conveyor 6', category: 'V:154', measure: 303.77445 },
    { group: 'Conveyor 6', category: 'V:156', measure: 2328.93745 }
  ];

  datasetLineChart: any = [
    { group: 'All', category: 2008, measure: 289309 },
    { group: 'All', category: 2009, measure: 234998 },
    { group: 'All', category: 2010, measure: 310900 },
    { group: 'All', category: 2011, measure: 223900 },
    { group: 'All', category: 2012, measure: 234500 },
    { group: 'Sam', category: 2008, measure: 81006.52 },
    { group: 'Sam', category: 2009, measure: 70499.4 },
    { group: 'Sam', category: 2010, measure: 96379 },
    { group: 'Sam', category: 2011, measure: 64931 },
    { group: 'Sam', category: 2012, measure: 70350 },
    { group: 'Peter', category: 2008, measure: 63647.98 },
    { group: 'Peter', category: 2009, measure: 61099.48 },
    { group: 'Peter', category: 2010, measure: 87052 },
    { group: 'Peter', category: 2011, measure: 58214 },
    { group: 'Peter', category: 2012, measure: 58625 },
    { group: 'Rick', category: 2008, measure: 23144.72 },
    { group: 'Rick', category: 2009, measure: 14099.88 },
    { group: 'Rick', category: 2010, measure: 15545 },
    { group: 'Rick', category: 2011, measure: 11195 },
    { group: 'Rick', category: 2012, measure: 11725 },
    { group: 'John', category: 2008, measure: 34717.08 },
    { group: 'John', category: 2009, measure: 30549.74 },
    { group: 'John', category: 2010, measure: 34199 },
    { group: 'John', category: 2011, measure: 33585 },
    { group: 'John', category: 2012, measure: 35175 },
    { group: 'Lenny', category: 2008, measure: 69434.16 },
    { group: 'Lenny', category: 2009, measure: 46999.6 },
    { group: 'Lenny', category: 2010, measure: 62180 },
    { group: 'Lenny', category: 2011, measure: 40302 },
    { group: 'Lenny', category: 2012, measure: 42210 },
    { group: 'Paul', category: 2008, measure: 7232.725 },
    { group: 'Paul', category: 2009, measure: 4699.96 },
    { group: 'Paul', category: 2010, measure: 6218 },
    { group: 'Paul', category: 2011, measure: 8956 },
    { group: 'Paul', category: 2012, measure: 9380 },
    { group: 'Steve', category: 2008, measure: 10125.815 },
    { group: 'Steve', category: 2009, measure: 7049.94 },
    { group: 'Steve', category: 2010, measure: 9327 },
    { group: 'Steve', category: 2011, measure: 6717 },
    { group: 'Steve', category: 2012, measure: 7035 }
  ];

  // set initial group value
  group: any = 'All';
  color: any = d3.scale.category20();
  dataset: any;
  constructor() {}

  ngOnInit() {}

  ngAfterContentInit() {
    const formatAsPercentage = d3.format('%'),
      formatAsPercentage1Dec: any = d3.format('.1%'),
      formatAsInteger: any = d3.format(','),
      fsec: any = d3.time.format('%S s'),
      fmin: any = d3.time.format('%M m'),
      fhou: any = d3.time.format('%H h'),
      fwee: any = d3.time.format('%a'),
      fdat: any = d3.time.format('%d d'),
      fmon: any = d3.time.format('%b');
    // d3.select('p').style('color', 'red');

    this.dsPieChart();
    this.dsBarChart();
    this.dsLineChart();
  }

  dsPieChart() {
    const dataset: any = [
      { category: 'Conveyor 1', measure: 0.3 },
      { category: 'Conveyor 2', measure: 0.25 },
      { category: 'Conveyor 3', measure: 0.15 },
      { category: 'Conveyor 4', measure: 0.05 },
      { category: 'Conveyor 5', measure: 0.18 },
      { category: 'Conveyor 6', measure: 0.04 }
    ];
    this.dataset = [
      { category: 'Conveyor 1', measure: 0.3 },
      { category: 'Conveyor 2', measure: 0.25 },
      { category: 'Conveyor 3', measure: 0.15 },
      { category: 'Conveyor 4', measure: 0.05 },
      { category: 'Conveyor 5', measure: 0.18 },
      { category: 'Conveyor 6', measure: 0.04 }
    ];
    const width: any = 400,
      height: any = 400,
      outerRadius: any = Math.min(width, height) / 2,
      innerRadius: any = outerRadius * 0.999,
      // for animation
      innerRadiusFinal: any = outerRadius * 0.5,
      innerRadiusFinal3: any = outerRadius * 0.45;
    this.color = d3.scale.category20(); // builtin range of colors
    const color: any = d3.scale.category20();

    const vis: any = d3
      .select('#pieChart')
      .append('svg:svg') // create the SVG element inside the <body>
      .data([dataset]) // associate our data with the document
      .attr('width', width) // set the width and height of our visualization (these will be attributes of the <svg> tag
      .attr('height', height)
      .append('svg:g') // make a group to hold our pie chart
      .attr('transform', 'translate(' + outerRadius + ',' + outerRadius + ')');
    // move the center of the pie chart from 0, 0 to radius, radius

    const arc: any = d3.svg
      .arc() // this will create <path> elements for us using arc data
      .outerRadius(outerRadius)
      .innerRadius(innerRadius);

    // for animation
    const arcFinal: any = d3.svg
      .arc()
      .innerRadius(innerRadiusFinal)
      .outerRadius(outerRadius);
    const arcFinal3: any = d3.svg
      .arc()
      .innerRadius(innerRadiusFinal3)
      .outerRadius(outerRadius);

    const pie: any = d3.layout
      .pie() // this will create arc data for us given a list of values
      .value(function(d: any) {
        return d.measure;
      }); // we must tell it out to access the value of each element in our data array

    const arcs: any = vis
      .selectAll('g.slice') // this selects all <g> elements with class slice (there aren't any yet)
      .data(pie) // associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties)
      .enter()
      // this will create <g> elements for every "extra" data element
      // that should be associated with a selection. The result is creating a <g> for every object in the data array
      .append('svg:g') // create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
      .attr('class', 'slice') // allow us to style things in the slices (like text)
      .on('mouseover', mouseover)
      .on('mouseout', mouseout)
      .on('click', (d: any, i: any) => {
        this.updateBarChart(d.data.category, color(i));
      });

    arcs
      .append('svg:path')
      .attr('fill', function(d, i) {
        return color(i);
      }) // set the color for each slice to be chosen from the color function defined above
      .attr('d', arc) // this creates the actual SVG path using the associated data (pie) with the arc drawing function
      .append('svg:title') // mouseover title showing the figures
      .text(function(d: any) {
        return d.category;
      });

    d3.selectAll('g.slice')
      .selectAll('path')
      .transition()
      .duration(750)
      .delay(10)
      .attr('d', arcFinal);

    // Add a label to the larger arcs, translated to the arc centroid and rotated.
    // source: http://bl.ocks.org/1305337#index.html
    arcs
      .filter(function(d: any) {
        return d.endAngle - d.startAngle > 0.2;
      })
      .append('svg:text')
      .attr('dy', '.35em')
      .attr('text-anchor', 'middle')
      .attr('transform', function(d: any) {
        return (
          'translate(' + arcFinal.centroid(d) + ')rotate(' + angle(d) + ')'
        );
      })
      // .text(function(d) { return formatAsPercentage(d.value); })
      .text(function(d: any) {
        return d.data.category;
      });

    // Computes the label angle of an arc, converting from radians to degrees.
    function angle(d: any) {
      const a = ((d.startAngle + d.endAngle) * 90) / Math.PI - 90;
      return a > 90 ? a - 180 : a;
    }

    // Pie chart title
    vis
      .append('svg:text')
      .attr('dy', '.35em')
      .attr('text-anchor', 'middle')
      .text('Conveyor')
      .attr('class', 'title');

    function mouseover() {
      d3.select(this)
        .select('path')
        .transition()
        .duration(750)
        // .attr("stroke","red")
        // .attr("stroke-width", 1.5)
        .attr('d', arcFinal3);
    }

    function mouseout() {
      d3.select(this)
        .select('path')
        .transition()
        .duration(750)
        // .attr("stroke","blue")
        // .attr("stroke-width", 1.5)
        .attr('d', arcFinal);
    }
  }

  up(d: any, i: any) {
    // const color: any = d3.scale.category20();
    /* update bar chart when user selects piece of the pie chart */
    this.updateBarChart(d.data.category, this.color(i));
    // this.updateLineChart(d.data.category, color(i), k);
  }

  /*
    ############# BAR CHART ###################
    -------------------------------------------
  */

  datasetBarChosen(group) {
    let ds = [];
    for (let x in this.datasetBarChart) {
      if (this.datasetBarChart[x].group == group) {
        ds.push(this.datasetBarChart[x]);
      }
    }
    return ds;
  }

  dsBarChartBasics() {
    const margin = { top: 30, right: 5, bottom: 20, left: 50 },
      width = 500 - margin.left - margin.right,
      height = 250 - margin.top - margin.bottom,
      colorBar = d3.scale.category20(),
      barPadding = 1;

    return {
      margin: margin,
      width: width,
      height: height,
      colorBar: colorBar,
      barPadding: barPadding
    };
  }

  dsBarChart() {
    const firstDatasetBarChart = this.datasetBarChosen(this.group);

    const basics = this.dsBarChartBasics();

    const margin = basics.margin,
      width = basics.width,
      height = basics.height,
      colorBar = basics.colorBar,
      barPadding = basics.barPadding;

    const xScale = d3.scale
      .linear()
      .domain([0, firstDatasetBarChart.length])
      .range([0, width]);

    // Create linear y scale
    // Purpose: No matter what the data is, the bar should fit into the svg area; bars should not
    // get higher than the svg height. Hence incoming data needs to be scaled to fit into the svg area.
    const yScale = d3.scale
      .linear()
      // use the max funtion to derive end point of the domain (max value of the dataset)
      // do not use the min value of the dataset as min of the domain as otherwise you will not see the first bar
      .domain([
        0,
        d3.max(firstDatasetBarChart, function(d) {
          return d.measure;
        })
      ])
      // As coordinates are always defined from the top left corner, the y position of the bar
      // is the svg height minus the data value. So you basically draw the bar starting from the top.
      // To have the y position calculated by the range function
      .range([height, 0]);

    // Create SVG element

    const svg = d3
      .select('#barChart')
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .attr('id', 'barChartPlot');

    const plot = svg
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    plot
      .selectAll('rect')
      .data(firstDatasetBarChart)
      .enter()
      .append('rect')
      .attr('x', function(d, i) {
        return xScale(i);
      })
      .attr('width', width / firstDatasetBarChart.length - barPadding)
      .attr('y', function(d: any) {
        return yScale(d.measure);
      })
      .attr('height', function(d: any) {
        return height - yScale(d.measure);
      })
      .attr('fill', 'lightgrey');

    // Add y labels to plot

    plot
      .selectAll('text')
      .data(firstDatasetBarChart)
      .enter()
      .append('text')
      .text(function(d) {
        return d3.round(d.measure);
      })
      .attr('text-anchor', 'middle')
      // Set x position to the left edge of each bar plus half the bar width
      .attr('x', function(d, i) {
        return (
          i * (width / firstDatasetBarChart.length) +
          (width / firstDatasetBarChart.length - barPadding) / 2
        );
      })
      .attr('y', function(d) {
        return yScale(d.measure) + 14;
      })
      .attr('class', 'yAxis');
    /* moved to CSS
    .attr("font-family", "sans-serif")
    .attr("font-size", "11px")
    .attr("fill", "white")
    */

    // Add x labels to chart

    const xLabels = svg
      .append('g')
      .attr(
        'transform',
        'translate(' + margin.left + ',' + (margin.top + height) + ')'
      );

    xLabels
      .selectAll('text.xAxis')
      .data(firstDatasetBarChart)
      .enter()
      .append('text')
      .text(function(d) {
        return d.category;
      })
      .attr('text-anchor', 'middle')
      // Set x position to the left edge of each bar plus half the bar width
      .attr('x', function(d: any, i: any) {
        return (
          i * (width / firstDatasetBarChart.length) +
          (width / firstDatasetBarChart.length - barPadding) / 2
        );
      })
      .attr('y', 15)
      .attr('class', 'xAxis');
    // .attr("style", "font-size: 12; font-family: Helvetica, sans-serif")

    // Title

    svg
      .append('text')
      .attr('x', (width + margin.left + margin.right) / 2)
      .attr('y', 15)
      .attr('class', 'title')
      .attr('text-anchor', 'middle')
      .text('Overall Status');
  }

  /* ** UPDATE CHART ** */

  /* updates bar chart on request */

  updateBarChart(group: any, colorChosen: any) {
    const currentDatasetBarChart = this.datasetBarChosen(group);

    const basics = this.dsBarChartBasics();

    const margin = basics.margin,
      width = basics.width,
      height = basics.height,
      colorBar = basics.colorBar,
      barPadding = basics.barPadding;

    const xScale = d3.scale
      .linear()
      .domain([0, currentDatasetBarChart.length])
      .range([0, width]);

    const yScale = d3.scale
      .linear()
      .domain([
        0,
        d3.max(currentDatasetBarChart, function(d) {
          return d.measure;
        })
      ])
      .range([height, 0]);

    const svg = d3.select('#barChart svg');

    const plot = d3.select('#barChartPlot').datum(currentDatasetBarChart);

    /* Note that here we only have to select the elements - no more appending! */
    plot
      .selectAll('rect')
      .data(currentDatasetBarChart)
      .transition()
      .duration(750)
      .attr('x', function(d, i) {
        return xScale(i);
      })
      .attr('width', width / currentDatasetBarChart.length - barPadding)
      .attr('y', function(d) {
        return yScale(d.measure);
      })
      .attr('height', function(d) {
        return height - yScale(d.measure);
      })
      .attr('fill', colorChosen);

    plot
      .selectAll('text.yAxis') // target the text element(s) which has a yAxis class defined
      .data(currentDatasetBarChart)
      .transition()
      .duration(750)
      .attr('text-anchor', 'middle')
      .attr('x', function(d, i) {
        return (
          i * (width / currentDatasetBarChart.length) +
          (width / currentDatasetBarChart.length - barPadding) / 2
        );
      })
      .attr('y', function(d) {
        return yScale(d.measure) + 14;
      })
      // .text(function(d) {
      //   return d3.round(d.measure);
      // })
      .attr('class', 'yAxis');

    svg
      .selectAll('text.title') // target the text element(s) which has a title class defined
      .attr('x', (width + margin.left + margin.right) / 2)
      .attr('y', 15)
      .attr('class', 'title')
      .attr('text-anchor', 'middle')
      .text(group + "'s overall status");
  }

  /*
  ############# LINE CHART ##################
  -------------------------------------------
  */

  datasetLineChartChosen(group) {
    const ds = [];
    for (let x in this.datasetLineChart) {
      if (this.datasetLineChart[x].group == group) {
        ds.push(this.datasetLineChart[x]);
      }
    }
    return ds;
  }

  dsLineChartBasics() {
    const margin = { top: 20, right: 10, bottom: 0, left: 50 },
      width = 500 - margin.left - margin.right,
      height = 150 - margin.top - margin.bottom;

    return {
      margin: margin,
      width: width,
      height: height
    };
  }

  dsLineChart() {
    const firstDatasetLineChart = this.datasetLineChartChosen(this.group);

    const basics = this.dsLineChartBasics();

    const margin = basics.margin,
      width = basics.width,
      height = basics.height;

    const xScale = d3.scale
      .linear()
      .domain([0, firstDatasetLineChart.length - 1])
      .range([0, width]);

    const yScale = d3.scale
      .linear()
      .domain([
        0,
        d3.max(firstDatasetLineChart, function(d) {
          return d.measure;
        })
      ])
      .range([height, 0]);

    const line = d3.svg
      .line()
      // .x(function(d) { return xScale(d.category); })
      .x(function(d, i) {
        return xScale(i);
      })
      .y(function(d: any) {
        return yScale(d.measure);
      });

    const svg = d3
      .select('#lineChart')
      .append('svg')
      .datum(firstDatasetLineChart)
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom);
    // create group and move it so that margins are respected (space for axis and title)

    const plot = svg
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
      .attr('id', 'lineChartPlot');

    /* descriptive titles as part of plot -- start */
    const dsLength = firstDatasetLineChart.length;

    plot
      .append('text')
      .text(firstDatasetLineChart[dsLength - 1].measure)
      .attr('id', 'lineChartTitle2')
      .attr('x', width / 2)
      .attr('y', height / 2);
    /* descriptive titles -- end */

    plot
      .append('path')
      .attr('class', 'line')
      .attr('d', line)
      // add color
      .attr('stroke', 'lightgrey');

    plot
      .selectAll('.dot')
      .data(firstDatasetLineChart)
      .enter()
      .append('circle')
      .attr('class', 'dot')
      // .attr("stroke", function (d) { return d.measure==datasetMeasureMin ? "red"
      // : (d.measure==datasetMeasureMax ? "green" : "steelblue") } )
      .attr('fill', function(d: any) {
        return d.measure ==
          d3.min(firstDatasetLineChart, function(d) {
            return d.measure;
          })
          ? 'red'
          : d.measure ==
            d3.max(firstDatasetLineChart, function(d) {
              return d.measure;
            })
            ? 'green'
            : 'white';
      })
      // .attr("stroke-width", function (d) { return d.measure==datasetMeasureMin || // //
      // d.measure==datasetMeasureMax ? "3px" : "1.5px"} )
      .attr(
        'cx',
        (): any => {
          return line.x();
        }
      )
      .attr(
        'cy',
        (): any => {
          return line.y();
        }
      )
      .attr('r', 3.5)
      .attr('stroke', 'lightgrey')
      .append('title')
      .text(function(d) {
        return d.category;
      });

    svg
      .append('text')
      .text('Performance')
      .attr('id', 'lineChartTitle1')
      .attr('x', margin.left + (width + margin.right) / 2)
      .attr('y', 10);
  }

  /* ** UPDATE CHART ** */

  /* updates bar chart on request */
  updateLineChart(group, colorChosen) {
    const currentDatasetLineChart = this.datasetLineChartChosen(group);

    const basics = this.dsLineChartBasics();

    const margin = basics.margin,
      width = basics.width,
      height = basics.height;

    const xScale = d3.scale
      .linear()
      .domain([0, currentDatasetLineChart.length - 1])
      .range([0, width]);

    const yScale = d3.scale
      .linear()
      .domain([
        0,
        d3.max(currentDatasetLineChart, function(d) {
          return d.measure;
        })
      ])
      .range([height, 0]);

    const line = d3.svg
      .line()
      .x(function(d, i) {
        return xScale(i);
      })
      .y(function(d: any) {
        return yScale(d.measure);
      });

    const plot = d3.select('#lineChartPlot').datum(currentDatasetLineChart);

    /* descriptive titles as part of plot -- start */
    const dsLength = currentDatasetLineChart.length;

    plot.select('text').text(currentDatasetLineChart[dsLength - 1].measure);
    /* descriptive titles -- end */

    plot
      .select('path')
      .transition()
      .duration(750)
      .attr('class', 'line')
      .attr('d', line)
      // add color
      .attr('stroke', colorChosen);

    const path = plot
      .selectAll('.dot')
      .data(currentDatasetLineChart)
      .transition()
      .duration(750)
      .attr('class', 'dot')
      .attr('fill', function(d) {
        return d.measure ==
          d3.min(currentDatasetLineChart, function(d) {
            return d.measure;
          })
          ? 'red'
          : d.measure ==
            d3.max(currentDatasetLineChart, function(d) {
              return d.measure;
            })
            ? 'green'
            : 'white';
      })
      .attr(
        'cx',
        (): any => {
          return line.x();
        }
      )
      .attr(
        'cy',
        (): any => {
          return line.y();
        }
      )
      .attr('r', 3.5)
      // add color
      .attr('stroke', colorChosen);

    path.selectAll('title').text(function(d) {
      return d.category;
    });
  }
}
