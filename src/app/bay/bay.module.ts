import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BayRoutingModule } from './bay-routing.module';
import { BayComponent } from './bay.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [CommonModule, BayRoutingModule, SharedModule],
  declarations: [BayComponent]
})
export class BayModule {}
