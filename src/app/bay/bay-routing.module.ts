import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BayComponent } from './bay.component';

const routes: Routes = [
  {
    path: '',
    component: BayComponent,
    data: {
      breadcrumb: 'Bay'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BayRoutingModule {}
