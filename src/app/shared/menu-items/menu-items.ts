import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Dashboards',
    main: [
      {
        state: 'dashboard',
        name: 'Overall Dashboard 1',
        type: 'link',
        icon: 'ti-home'
      },
      {
        state: 'bay',
        name: 'Overall Dashboard 2',
        type: 'link',
        icon: 'ti-blackboard'
      }
    ]
  },
  {
    label: 'Adminstration',
    main: [
      {
        state: 'manageroles',
        name: 'Manage Roles',
        type: 'link',
        icon: 'ti-layout-list-thumb'
      },
      {
        state: 'manageusers',
        name: 'Manage Users',
        type: 'link',
        icon: 'ti-user'
      },
      {
        state: 'managepermissions',
        name: 'Manage Permissions',
        type: 'link',
        icon: 'ti-id-badge'
      }
    ]
  },
  {
    label: 'Other Info',
    main: [
      {
        state: 'changepassword',
        name: 'Change Password',
        type: 'link',
        icon: 'ti-key'
      }
    ]
  }
];

//   {
//   state: 'user',
//   name: 'User Profile',
//   type: 'sub',
//   icon: 'ti-user',
//   children: [
//     {
//       state: 'profile',
//       name: 'User Profile'
//     }
//   ]
// }
//     ]
//   }
// ];

/*
*/

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
